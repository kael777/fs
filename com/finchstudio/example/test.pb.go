// Code generated by protoc-gen-go.
// source: test.proto
// DO NOT EDIT!

/*
Package example is a generated protocol buffer package.

It is generated from these files:
	test.proto
	test2.proto

It has these top-level messages:
	Test
	FSNoob
*/
package example

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
const _ = proto.ProtoPackageIsVersion1

type Test_PhoneType int32

const (
	Test_MOBILE Test_PhoneType = 0
	Test_HOME   Test_PhoneType = 1
	Test_WORK   Test_PhoneType = 2
)

var Test_PhoneType_name = map[int32]string{
	0: "MOBILE",
	1: "HOME",
	2: "WORK",
}
var Test_PhoneType_value = map[string]int32{
	"MOBILE": 0,
	"HOME":   1,
	"WORK":   2,
}

func (x Test_PhoneType) String() string {
	return proto.EnumName(Test_PhoneType_name, int32(x))
}
func (Test_PhoneType) EnumDescriptor() ([]byte, []int) { return fileDescriptor0, []int{0, 0} }

type Test struct {
	Label  string              `protobuf:"bytes,1,opt,name=label" json:"label,omitempty"`
	Type   int32               `protobuf:"varint,2,opt,name=type" json:"type,omitempty"`
	Reps   []int64             `protobuf:"varint,3,rep,name=reps" json:"reps,omitempty"`
	Phones []*Test_PhoneNumber `protobuf:"bytes,4,rep,name=phones" json:"phones,omitempty"`
}

func (m *Test) Reset()                    { *m = Test{} }
func (m *Test) String() string            { return proto.CompactTextString(m) }
func (*Test) ProtoMessage()               {}
func (*Test) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{0} }

func (m *Test) GetPhones() []*Test_PhoneNumber {
	if m != nil {
		return m.Phones
	}
	return nil
}

type Test_PhoneNumber struct {
	Number string         `protobuf:"bytes,1,opt,name=number" json:"number,omitempty"`
	Type   Test_PhoneType `protobuf:"varint,2,opt,name=type,enum=example.Test_PhoneType" json:"type,omitempty"`
}

func (m *Test_PhoneNumber) Reset()                    { *m = Test_PhoneNumber{} }
func (m *Test_PhoneNumber) String() string            { return proto.CompactTextString(m) }
func (*Test_PhoneNumber) ProtoMessage()               {}
func (*Test_PhoneNumber) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{0, 0} }

func init() {
	proto.RegisterType((*Test)(nil), "example.Test")
	proto.RegisterType((*Test_PhoneNumber)(nil), "example.Test.PhoneNumber")
	proto.RegisterEnum("example.Test_PhoneType", Test_PhoneType_name, Test_PhoneType_value)
}

var fileDescriptor0 = []byte{
	// 192 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x09, 0x6e, 0x88, 0x02, 0xff, 0xe2, 0xe2, 0x2a, 0x49, 0x2d, 0x2e,
	0xd1, 0x2b, 0x28, 0xca, 0x2f, 0xc9, 0x17, 0x62, 0x4f, 0xad, 0x48, 0xcc, 0x2d, 0xc8, 0x49, 0x55,
	0x3a, 0xcf, 0xc8, 0xc5, 0x12, 0x02, 0x14, 0x17, 0xe2, 0xe5, 0x62, 0xcd, 0x49, 0x4c, 0x4a, 0xcd,
	0x91, 0x60, 0x54, 0x60, 0xd4, 0xe0, 0x14, 0xe2, 0xe1, 0x62, 0x29, 0xa9, 0x2c, 0x48, 0x95, 0x60,
	0x02, 0xf2, 0x58, 0x41, 0xbc, 0xa2, 0xd4, 0x82, 0x62, 0x09, 0x66, 0x05, 0x66, 0x0d, 0x66, 0x21,
	0x4d, 0x2e, 0xb6, 0x82, 0x8c, 0xfc, 0xbc, 0xd4, 0x62, 0x09, 0x16, 0x20, 0x9f, 0xdb, 0x48, 0x52,
	0x0f, 0x6a, 0x9a, 0x1e, 0xc8, 0x24, 0xbd, 0x00, 0x90, 0x9c, 0x5f, 0x69, 0x6e, 0x52, 0x6a, 0x91,
	0x94, 0x0b, 0x17, 0x37, 0x12, 0x57, 0x88, 0x8f, 0x8b, 0x2d, 0x0f, 0xcc, 0x82, 0xda, 0xa2, 0x8a,
	0x64, 0x0b, 0x9f, 0x91, 0x38, 0x16, 0x73, 0x42, 0x80, 0xd2, 0x4a, 0xda, 0x5c, 0x9c, 0x70, 0x8e,
	0x10, 0x17, 0x17, 0x9b, 0xaf, 0xbf, 0x93, 0xa7, 0x8f, 0xab, 0x00, 0x83, 0x10, 0x07, 0x17, 0x8b,
	0x87, 0xbf, 0xaf, 0xab, 0x00, 0x23, 0x88, 0x15, 0xee, 0x1f, 0xe4, 0x2d, 0xc0, 0x94, 0xc4, 0x06,
	0xf6, 0xa1, 0x31, 0x20, 0x00, 0x00, 0xff, 0xff, 0x4b, 0x60, 0xb6, 0x81, 0xef, 0x00, 0x00, 0x00,
}
