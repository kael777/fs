package FSHttpUtil

import (
	 "github.com/gin-gonic/gin"
)

type METHOD uint8
const (
	GET METHOD = iota
	POST
	PUT
	DELETE
	DAMN
)

type HandleFunc func(c *gin.Context)

type FuncionUnit struct  {
	Method uint8
	Handle HandleFunc
}

type RouteTable map[string]*FuncionUnit

type HttpKit struct  {
	R RouteTable
}

var Hk *HttpKit

func init()  {
	Hk = &HttpKit{}
	Hk.R = make(RouteTable)
}

func (srv *HttpKit) DoRouter(method uint8,path string,cb HandleFunc){
	srv.R[path] = &FuncionUnit{method,cb}
}

func (srv *HttpKit) ServerMainHttp(ip string,middlewares []*gin.HandlerFunc, cleanup func()){
	router := gin.Default()
	if middlewares != nil{
		for i:=0;i<len(middlewares);i++  {
			router.Use(*middlewares[i])
		}
	}
	for k,v := range(srv.R){
		if v.Method == GET {
			router.GET(k,v.Handle)
		}else if v.Method == POST{
			router.POST(k,v.Handle)
		}
	}
	router.Run(ip)
	if cleanup!=nil {
		cleanup()
	}
}

type CommonSetup interface  {
	ConfigLoad()
	DataBaseSetup()
	PreLoadDatabaseData()
	Loop()
	Cleanup()
}

func ServerSetup(cs CommonSetup)  {
	cs.ConfigLoad()
	cs.DataBaseSetup()
	cs.PreLoadDatabaseData()
	cs.Loop()
	cs.Cleanup()
}

//type ServerEnv struct {
//	a int
//	b int
//}
//func (*ServerEnv)ConfgLoad()  {
//}
//func (*ServerEnv)DataBaseSetup()  {
//}
//func (*ServerEnv)PreLoadDatabaseData()  {
//}
//func (*ServerEnv)Loop()  {
//}
//func (s *ServerEnv)Cleanup()  {
//}
//var s ServerEnv
//ServerSetup(s)