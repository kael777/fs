package FSCodec

type Codec interface {
	/**
	编码
	param：数据,命令ID,秘钥
	result：加密后数据,错误
	**/
	Encode(interface{},uint16,[]byte)([]byte,error) // encoded data,error
	/**
	解码
	param：数据,秘钥
	result：解密后数据,命令ID,错误
	**/
	Decode([]byte,[]byte)(interface{},uint16,error) // data,command id,error
}

var Cryp Crypto
var GlobalEncryptKey []byte
func init(){

}
