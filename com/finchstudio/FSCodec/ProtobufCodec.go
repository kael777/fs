package FSCodec
import (
	"com/finchstudio/FSUtils/binary"
	"github.com/golang/protobuf/proto"
	"errors"
)

type ProtobufCodec struct {

}

func (pb *ProtobufCodec)Encode(buf interface{},cmd uint16,key []byte)([]byte,error){
	// protobuf marshal as data buffer
	dataCMD := []byte{0,0}
	binary.PutUint16LE(dataCMD,cmd)
	var dataInf proto.Message
	dataInf = buf.(proto.Message)
	sendback,_:=  proto.Marshal(dataInf)
	// just encrypt buf
	encrypt := Cryp.Encrypt(sendback,key)
	encrypt = append(dataCMD[:],encrypt...)
	return encrypt,nil
}

func (pb *ProtobufCodec)Decode(buf []byte,key []byte)(interface{},uint16,error){
	if len(buf) <2 {
		return nil,0,errors.New("wrong message")
	}
	// decrypt buffer
	decrypted := Cryp.Decrypt(buf,key)
//	decrypted:=buf
	// read first 2 byte as cmd
	cmd := binary.GetUint16LE(decrypted)
	// route and send data to detail handler function to unmashal as struct
	return decrypted[2:],cmd,nil
}
