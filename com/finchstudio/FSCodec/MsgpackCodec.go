package FSCodec
import (
	"gopkg.in/vmihailenco/msgpack.v2"
	"errors"
	"com/finchstudio/FSUtils/binary"
)

type MsgpackCodec struct {

}

func init() {
}

func (pb *MsgpackCodec)Encode(input interface{},cmd uint16,key []byte)([]byte,error){
	dataCMD := []byte{0,0}
	binary.PutUint16LE(dataCMD,cmd)
	sendback,err:= msgpack.Marshal(input)
	// just encrypt buf
	encrypt := Cryp.Encrypt(sendback,key)
	encrypt = append(dataCMD[:],encrypt...)
	return encrypt,err
}

func (pb *MsgpackCodec)Decode(buf []byte,key []byte)(interface{},uint16,error){
	if len(buf) <2 {
		return nil,0,errors.New("wrong message")
	}
	// decrypt buffer
	decrypted := Cryp.Decrypt(buf,key)
	//	decrypted:=buf
	// read first 2 byte as cmd
	cmd := binary.GetUint16LE(decrypted)
	// route and send data to detail handler function to unmashal as struct
	return decrypted[2:],cmd,nil
}
