package FSCodec

type Crypto interface {
	/**
	编码
	param：数据,秘钥
	result：加密后数据
	**/
	Encrypt([]byte,[]byte)([]byte)
	/**
	解码
	param：数据,秘钥
	result：解密后数据
	**/
	Decrypt([]byte,[]byte)([]byte)
}