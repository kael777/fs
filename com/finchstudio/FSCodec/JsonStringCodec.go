package FSCodec
import (
	"github.com/bitly/go-simplejson"
	"errors"
//	"encoding/json"
)

type JsonStringCodec struct {

}

func (pb *JsonStringCodec)Encode(input interface{},_ uint16,key []byte)([]byte,error){
	// way 1
//	return json.Marshal(input.(map[string]interface{}))

	// way 2
	data := input.(map[string]interface{})
	JS:=simplejson.New()
	for k,v := range(data){
		JS.Set(k,v)
	}
	encrypt,err := JS.Encode()
	encrypt = Cryp.Encrypt(encrypt,key)
	return encrypt,err
}

func (pb *JsonStringCodec)Decode(data []byte,key []byte)(interface{},uint16,error){
	// way 1
//	output := make(map[string]interface{})
//	err := json.Unmarshal(data, &output)
//	if err!=nil {
//		return nil,-1,err
//	}else{
//		if id,ok:=output["cmd"];!ok{
//			return nil,-1,errors.New("wrong command id")
//		}else {
//			return output,int(id.(float64)),nil
//		}
//	}

	// way 2
	data = Cryp.Decrypt(data,key)
	cfg,err:=simplejson.NewJson(data)
	if err!=nil{
		return nil,0,err
	}else{
		if node,ok:=cfg.CheckGet("cmd");!ok{
			return nil,0,errors.New("wrong command id")
		}else{
			val,_:=node.Int()
			return node,uint16(val),nil
		}
	}
}
