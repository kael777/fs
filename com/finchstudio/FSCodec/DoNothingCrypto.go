package FSCodec

type DoNothingCrypto struct  {

}

func (d*DoNothingCrypto)Encrypt(input []byte,key []byte)([]byte){
	return input
}

func (d*DoNothingCrypto)Decrypt(input []byte,key []byte)([]byte){
	return input
}
