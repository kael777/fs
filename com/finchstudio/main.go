package main
import "com/finchstudio/FSUtils/Config"
//import "strconv"
import (
	"runtime"
	"fmt"
//	"net"
	"com/finchstudio/FSNet"
	"github.com/bitly/go-simplejson"
	"com/finchstudio/FSUtils/binary"
	"com/finchstudio/example"
	"github.com/golang/protobuf/proto"
	"gopkg.in/vmihailenco/msgpack.v2"
)

// input
func abc(con *FSNet.ClientConn, inputdata interface{}) (interface{},uint16) {
	output:=make(map[string]interface{})
	output["xxx"] = 3
	output["cmd"] = 3
	output["jb"],_ = inputdata.(*simplejson.Json).CheckGet("jb")
//	rrr["jb"],_ = p1.(map[string]interface {})["jb"]
	// do logic blabla
	return output,1
}

// input
func abc3(con *FSNet.ClientConn, inputdata interface{}) (interface{},uint16 ){
	// unmarshal your self here by doing detail logic
	newTest := &example.FSNoob{}
	proto.Unmarshal(inputdata.([]byte),newTest)
	output := &example.Test{Label:newTest.Jb,Type:3}
	return output,uint16(newTest.Cmd)
}

type ts struct {
	Cmd   uint16
	Cmd1   string
	Cmd2   string
	Cmd3   string
	Cmd4   string
	Cmd5   string
	Cmd6   string
	Cmd7   string
	Cmd8   string
	Cmd9   string
	Cmd0   string
	Jb   string
}

type tsOut struct {
	Cmd   uint16
	Cmd11   string
	Jb   string
}
// input
func abc5(con *FSNet.ClientConn, inputdata interface{}) (interface{},uint16 ){
	// unmarshal your self here by doing detail logic
	newTest := &ts{}
	msgpack.Unmarshal(inputdata.([]byte),&newTest)
	newTest.Cmd0 = "322"
	newTest.Cmd1 = "233"
	newTest.Jb = "wrong"
	output:= &tsOut{Cmd:3,Cmd11:newTest.Cmd1,Jb:newTest.Jb}
	return output,uint16(newTest.Cmd)
}

func main()  {
//	FSNet.RegisterFunc(uint16(1),abc)
//	msg:="{\"cmd\":1,\"jb\":3}"
//	data,cmd_id,_:=FSNet.Sk.Codec.Decode([]byte(msg),nil)
//	function, _ := FSNet.MainRouter.M[cmd_id];
//	result,_:=function(nil,data)
//
//	sendback,_:= FSNet.Sk.Codec.Encode(result,uint16(2),nil)
//	dataLenByte := []byte{0,0,0,0}
//	binary.PutUint32LE(dataLenByte,uint32(len(sendback)))
//	sendback = append(dataLenByte[:], []byte(sendback)...)
//	fmt.Println(sendback)

	// protobuf
	test := &example.FSNoob{        // 使用辅助函数设置域的值
		1,2,3,4,5,6,7,8,9,10,"jb",
	}    // 进行编码
	sourceByte, err := proto.Marshal(test); if err != nil {
	}
	cmdbyte := []byte{1,0}
	sourceByte = append(cmdbyte,sourceByte...)
	FSNet.RegisterFunc(1,abc3)
	data,cmd_id,_:=FSNet.Sk.Codec.Decode([]byte(sourceByte),nil)
	function, _ := FSNet.MainRouter.M[cmd_id];
	result,_:=function(nil,data)
	sendback,_:= FSNet.Sk.Codec.Encode(result,uint16(2),nil)
	dataLenByte := []byte{0,0,0,0}
	binary.PutUint32LE(dataLenByte,uint32(len(sendback)))
	sendback = append(dataLenByte[:], []byte(sendback)...)
	fmt.Println(sendback)

	runtime.GOMAXPROCS(runtime.NumCPU())
	Config.LoadConfig("config.json")
	remotePath ,_:=Config.Config.Get("remotePath").String()
	fmt.Println(remotePath)
	fmt.Println("svn://liuqiang:liuqiang@192.168.1.2/svnfile")

	t:=100
	if t > 10 {
		goto A
	} else {
		goto B
	}
	A:{
		fmt.Println("A")
		goto C
	}

	B:{
		fmt.Println("B")
	}
	C:

}
