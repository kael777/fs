package main
import "testing"
import "com/finchstudio/FSUtils/Log"
//import "com/finchstudio/FSUtils/Config"
import "strconv"
import "runtime"
import (
//	"com/finchstudio/FSUtils/Crypto"
//	"com/finchstudio/FSUtils/Crypto/mt19937"
//	"com/finchstudio/FSUtils/Crypto/dh64"
	"com/finchstudio/FSNet"
//	"github.com/bitly/go-simplejson"
	"com/finchstudio/FSUtils/binary"
	"gopkg.in/vmihailenco/msgpack.v2"
//	"com/finchstudio/example"
//	"github.com/golang/protobuf/proto"
//	"net"
)
type BigStruct struct {
	C01 uint64
	C02 uint64
	C03 uint64
	C04 uint64
	C05 uint64
	C06 uint64
	C07 uint64
	C08 uint64
	C09 uint64
	C10 uint64
	C11 uint64
	C12 uint64
	C13 uint64
	C14 uint64
	C15 uint64
	C16 uint64
	C17 uint64
	C18 uint64
	C19 uint64
	C20 uint64
	C21 uint64
	C22 uint64
	C23 uint64
	C24 uint64
	C25 uint64
	C26 uint64
	C27 uint64
	C28 uint64
	C29 uint64
	C30 uint64
}
//import "time"
func log(i int) {
	Log.Debug("Debug>>>>>>>>>>>>>>>>>>>>>>" + strconv.Itoa(i))
	Log.Info("Info>>>>>>>>>>>>>>>>>>>>>>>>>" + strconv.Itoa(i))
	Log.Warn("Warn>>>>>>>>>>>>>>>>>>>>>>>>>" + strconv.Itoa(i))
	Log.Error("Error>>>>>>>>>>>>>>>>>>>>>>>>>" + strconv.Itoa(i))
	Log.Fatal("Fatal>>>>>>>>>>>>>>>>>>>>>>>>>" + strconv.Itoa(i))
}

func Test(t *testing.T) {
	runtime.GOMAXPROCS(runtime.NumCPU())
	Log.SetConsole(true)
	//指定日志文件备份方式为文件大小的方式
	//第一个参数为日志文件存放目录
	//第二个参数为日志文件命名
	//第三个参数为备份文件最大数量
	//第四个参数为备份文件大小
	//第五个参数为文件大小的单位
	//	Log.SetRollingFile(".", "test.log", 10, 5, Log.KB)
	//指定日志文件备份方式为日期的方式
	//第一个参数为日志文件存放目录
	//第二个参数为日志文件命名
	Log.SetRollingDaily(".", "test.log")

	//指定日志级别  ALL，DEBUG，INFO，WARN，ERROR，FATAL，OFF 级别由低到高
	//一般习惯是测试阶段为debug，生成环境为info以上
	Log.SetLevel(Log.FATAL)

	for i := 10000; i > 0; i-- {
		log(i)
		//time.Sleep(1 * time.Millisecond)
	}
//	time.Sleep(1 * time.Second)
}


// benchmarks
//func Benchmark_logger(b *testing.B) {
//	Log.SetConsole(true)
//	Log.SetRollingDaily(".", "test.log")
//	Log.SetLevel(Log.FATAL)
//	for i := 10000; i > 0; i-- {
//		log(i)
//	}
//}

//func Benchmark_Config(b *testing.B) {
//	Config.LoadConfig("config.json")
//	for i := 10000; i > 0; i-- {
//		remotePath ,_:=Config.Config.Get("remotePath").String()
//		a:=len(remotePath)
//		a++
//	}
//}

//func Benchmark_xxtea(b *testing.B) {
//	str:="1234567890!@#$%^&*()_+abcdefghijklmnopqrstuvwxyz"
//	password:="heisenberg"
//	for i := 10000; i > 0; i-- {
//		enc:=Crypto.XXTeaEncrypt([]byte(str),[]byte(password))
//		Crypto.XXTeaDecrypt(enc,[]byte(password))
//	}
//}

//func Benchmark_MT19937_Uint64(b *testing.B) {
//	mt := mt19937.New()
//	b.SetBytes(8)
//	b.ResetTimer()
//	for i := 0; i < b.N; i++ {
//		mt.Uint64()
//	}
//}

//func Benchmark_dh(b *testing.B) {
//	for i := 0; i < b.N; i++ {
//		srv_pub,srv_pri := dh64.KeyPair()
//		cli_pub,cli_pri := dh64.KeyPair()
//		s_server:=dh64.Secret(srv_pri,cli_pub)
//		s_client:=dh64.Secret(cli_pri,srv_pub)
//		if s_server==s_client{
//
//		}
//	}
//}

func i1(c *BigStruct) {
//	v:= c.C30+1
//	return v
	c.C30++
}
func i2(c interface{}) {
	c.(*BigStruct).C30++
}
func i3(c BigStruct){
	c.C30++
}

func Benchmark_InterfaceKnow(b *testing.B) {
	var a = new(BigStruct)
	for i := 0; i < b.N; i++ {
		i1(a)
	}
}

func Benchmark_InterfacePass(b *testing.B) {
	var a = new(BigStruct)
	for i := 0; i < b.N; i++ {
		i2(a)
	}
}

func Benchmark_InterfacePassValue(b *testing.B) {
	var a = new(BigStruct)
	for i := 0; i < b.N; i++ {
		i3(*a)
	}
}

//func Benchmark_jsonEncode(b *testing.B)  {
//	msg:="{\"cmd\":1,\"jb\":3,\"cmd2\":1,\"cmd3\":1,\"cmd4\":1,\"cmd13\":1,\"cmd14\":1,\"cmd23\":1,\"cmd24\":1,\"cmd33\":1,\"cmd34\":1}"
//	FSNet.RegisterFunc(1,abc)
//	for i := 0; i < b.N; i++ {
//		data,cmd_id,_:=FSNet.Sk.Codec.Decode([]byte(msg),nil)
//		function, _ := FSNet.MainRouter.M[cmd_id];
//		result,_:=function(nil,data)
//		sendback,_:= FSNet.Sk.Codec.Encode(result,uint16(2),nil)
//		dataLenByte := []byte{0,0,0,0}
//		binary.PutUint32LE(dataLenByte,uint32(len(sendback)))
//		sendback = append(dataLenByte[:], []byte(sendback)...)
//	}
//}

//func Benchmark_pbEncode(b *testing.B)  {
//	test := &example.FSNoob{        // 使用辅助函数设置域的值
//		1,2,3,4,5,6,7,8,9,10,"jb",
//	}    // 进行编码
//	sourceByte, err := proto.Marshal(test); if err != nil {
//	}
//	cmdbyte := []byte{1,0}
//	sourceByte = append(cmdbyte,sourceByte...)
//	FSNet.RegisterFunc(1,abc3)
//	for i := 0; i < b.N; i++ {
////		// 进行解码
////		newTest := &example.FSNoob{}
////		err = proto.Unmarshal(data, newTest)  ;  if err != nil {
////		}
////		sendbackstruct:=abc3(nil,newTest)
////
////		var dataInf proto.Message
////		dataInf = sendbackstruct.(proto.Message)
////		// 测试结果
////		sendback,_:=  proto.Marshal(dataInf)
////		dataLenByte := []byte{0,0,0,0}
////		binary.PutUint32LE(dataLenByte,uint32(len(sendback)))
////		sendback = append(dataLenByte[:], []byte(sendback)...)
//		data,cmd_id,_:=FSNet.Sk.Codec.Decode([]byte(sourceByte),nil)
//		function, _ := FSNet.MainRouter.M[cmd_id];
//		result,_:=function(nil,data)
////		sendback,_:= FSNet.Sk.Codec.Encode(result,uint16(2),nil)
//		FSNet.Sk.Codec.Encode(result,uint16(2),nil)
////		dataLenByte := []byte{0,0,0,0}
////		binary.PutUint32LE(dataLenByte,uint32(len(sendback)))
////		sendback = append(dataLenByte[:], []byte(sendback)...)
//	}
//}

func Benchmark_msgpackEncode(b *testing.B)  {
	FSNet.RegisterFunc(1,abc5)
	msg := &ts{Cmd:1,
		Cmd0:"333",
		Cmd1:"333",
		Cmd2:"333",
		Cmd3:"333",
		Cmd4:"333",
		Cmd5:"333",
		Cmd6:"333",
		Cmd7:"333",
		Cmd8:"333",
		Cmd9:"333",
		Jb:"333SADA",
	}
	byt, _ := msgpack.Marshal(msg)
	cmdbyte := []byte{1,0}
	byt = append(cmdbyte,byt...)

	for i := 0; i < b.N; i++ {
		data,cmd_id,_:=FSNet.Sk.Codec.Decode(byt,nil)
		function, _ := FSNet.MainRouter.M[cmd_id];
		result,_:=function(nil,data)
		sendback,_:= FSNet.Sk.Codec.Encode(result,uint16(2),nil)
		dataLenByte := []byte{0,0,0,0}
		binary.PutUint32LE(dataLenByte,uint32(len(sendback)))
		sendback = append(dataLenByte[:], []byte(sendback)...)
	}
}