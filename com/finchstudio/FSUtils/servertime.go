package FSUtils

import "time"

var serverRunTime uint64

func init(){
	serverRunTime = time.Now().Unix()
}

func GetRunSecond() uint64{
	return time.Now().Unix() - serverRunTime
}
