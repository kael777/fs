package Config

import (
	"github.com/bitly/go-simplejson"
	"os"
)

//Config 配置文件
var Config *simplejson.Json

//LoadConfig 配置文件初始化函数,在程序启动时候调用
func LoadConfig(filename string) error {
	buf := make([]byte, 1024)
	f, err := os.Open(filename)
	if err != nil {
		return err
	}
	f.Read(buf)
	//
	Config, _ = simplejson.NewJson(buf)
	f.Close()
	return nil
}