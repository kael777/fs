package FSUtils
import (
	"math/rand"
	"time"
	"fmt"
	"os"
	"net/smtp"
	"strings"
	"io/ioutil"
	"path/filepath"
)

//GeneratePIDFile 生成一个pID文件，格式：<name>.<ID>.pID
func GeneratePIDFile(name string, id int) {
	var filename string
	if id == 0 {
		filename = fmt.Sprintf("%s.pid", name)
	} else {
		filename = fmt.Sprintf("%s.%d.pid", name, id)
	}

	f, err := os.OpenFile(filename, os.O_WRONLY|os.O_TRUNC|os.O_CREATE, 0666)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	pid := os.Getpid()
	fmt.Fprintf(f, "%d", pid)
//	Debug("new %s[%d], pID: %d", name, id, pid)
}

var r *rand.Rand

func GenRandomString(n int)string{
	str := "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	bytes := []byte(str)
	result := []byte{}
	if r == nil{
		r = rand.New(rand.NewSource(time.Now().Unix()))
	}
	for i:=0;i<n;i++{
		result = append(result,bytes[r.Intn(len(bytes))])
	}
	return string(result)
}

func GetRandomRange(_min,_max int) int{
	if r == nil{
		r = rand.New(rand.NewSource(time.Now().Unix()))
	}
	return r.Intn(_max-_min+1)+_min
}

//GetRandom 生成一个1-n的随机数
func GetRandom(_max int) int {
	if r == nil {
		r = rand.New(rand.NewSource(time.Now().UnixNano()))
	}
	return r.Intn(_max) + 1
}

func TimeToStr(t uint64) string{
	return time.Unix(t,0).Format("2006-01-02 15:04:05")
}

func StrToTime(str string) int64 {
	loc, _ := time.LoadLocation("Local")
	the_time, err := time.ParseInLocation("2006-01-02 15:04:05", str, loc)
	if err == nil {
		return the_time.Unix()
	} else {
		return 0
	}
}

//NextTime 当前时间下一个指定时间
func NextTime(h, m, s int) int64 {
	now := time.Now()
	year, mon, day := now.Date()
	hour, min, sec := now.Clock()

	ts := StrToTime(fmt.Sprintf("%04d-%02d-%02d %02d:%02d:%02d", year, mon, day, 0, 0, 0))

	if (3600*h + 60*m + s) <= (3600*hour + 60*min + sec) {
		ts += int64(3600*24 + 3600*h + 60*m + s)
	} else {
		ts += int64(3600*h + 60*m + s)
	}
	return ts
}

//LowerCasedName 小写字符串
func LowerCasedName(name string) string {
	newstr := make([]rune, 0)
	firstTime := true

	for _, chr := range name {
		if isUpper := 'A' <= chr && chr <= 'Z'; isUpper {
			if firstTime == true {
				firstTime = false
			} else {
				newstr = append(newstr, '_')
			}
			chr -= ('A' - 'a')
		}
		newstr = append(newstr, chr)
	}
	return string(newstr)
}

//UpperCasedName 大写字符串
func UpperCasedName(name string) string {
	newstr := make([]rune,0)
	upNextChar := true
	for _, chr := range name {
		switch {
		case upNextChar:
			upNextChar = false
			chr -= ('a' - 'A')
		case chr == '_':
			upNextChar = true
			continue
		}
		newstr = append(newstr, chr)
	}
	return string(newstr)
}

/**
发送邮件
	param：to 发送给谁，比如：example@example.com;example1@163.com;example2@sina.com.cn;...
	param：user : example@example.com login smtp server user
	password: xxxxx login smtp server password
	host: smtp.example.com:port   smtp.163.com:25
	to: example@example.com;example1@163.com;example2@sina.com.cn;...
	subject:The subject of mail
	body: The content of mail
	mailtyoe: mail type html or text
	result：error 错误对象
**/
func SendEmail(to string ,user string,password string,host string,subject string,body string,mailtype string) error{
	hp := strings.Split(host, ":")
	auth := smtp.PlainAuth("", user, password, hp[0])
	var content_type string
	if mailtype == "html" {
		content_type = "Content-Type: text/" + mailtype + "; charset=UTF-8"
	} else {
		content_type = "Content-Type: text/plain" + "; charset=UTF-8"
	}
	msg := []byte("To: " + to + "\r\nFrom: " + user + "<" + user + ">\r\nSubject: " + subject + "\r\n" + content_type + "\r\n\r\n" + body)
	send_to := strings.Split(to, ";")
	err := smtp.SendMail(host, auth, user, send_to, msg)
	return err
}

/**
从int32转化为[]byte
**/
func Uint32ToBytes(i uint32) []byte {
	return []byte{byte((i >> 24) & 0xff), byte((i >> 16) & 0xff),
		byte((i >> 8) & 0xff), byte(i & 0xff)}
}

/**
从[]byte转化为int32
**/
func BytesToUint32(buf []byte) uint32 {
	return uint32(buf[0])<<24 + uint32(buf[1])<<16 + uint32(buf[2])<<8 +
	uint32(buf[3])
}


//获取指定目录下的所有文件/文件夹，不进入下一级目录搜索，可以匹配后缀过滤。
func ListDir(dirPth string, suffix string,fileOrDir bool) (files []string, err error) {
	files = make([]string, 0, 10)
	dir, err := ioutil.ReadDir(dirPth)
	if err != nil {
		return nil, err
	}
	PthSep := string(os.PathSeparator)
	suffix = strings.ToUpper(suffix) //忽略后缀匹配的大小写
	for _, fi := range dir {
		if fileOrDir {
			// 列出文件
			if fi.IsDir() { // 忽略目录
				continue
			}
		}else{
			// 列出文件夹
			if !fi.IsDir() { // 忽略目录
				continue
			}
		}

		if strings.HasSuffix(strings.ToUpper(fi.Name()), suffix) { //匹配文件
			files = append(files, dirPth+PthSep+fi.Name())
		}
	}
	return files, nil
}
//获取指定目录及所有子目录下的所有文件，可以匹配后缀过滤。
func WalkDir(dirPth, suffix string) (files []string, err error) {
	files = make([]string, 0, 30)
	suffix = strings.ToUpper(suffix) //忽略后缀匹配的大小写
	err = filepath.Walk(dirPth, func(filename string, fi os.FileInfo, err error) error { //遍历目录
		//if err != nil { //忽略错误
		// return err
		//}
		if fi.IsDir() { // 忽略目录
			return nil
		}
		if strings.HasSuffix(strings.ToUpper(fi.Name()), suffix) {
			files = append(files, filename)
		}
		return nil
	})
	return files, err
}

func FileExist(path string) bool{
	_, err := os.Stat(path)
	return (err==nil)
}