package memory

import (
	"reflect"
	"sync/atomic"
	"unsafe"
)

/*
pool
 -|
  | slab ...
  |   -| slab Object ...

*/

type SlabObject struct{
	mem []byte
	next unsafe.Pointer
}

type Slab struct {
	eachObjectSize int
	page []byte
	objects []SlabObject
	beginPtr uintptr
	endPtr uintptr
	currentHead unsafe.Pointer
}

type Pool struct {
	slabs []Slab
	minSize int
	maxSize int
}

func NewPool(minSize,maxSize,factor,pageSize int)*Pool{
	pool := &Pool{
		make([]Slab,0,10),minSize,maxSize,
	}
	currentSlabObjectSize := minSize
	for {
		s := Slab{
			eachObjectSize:currentSlabObjectSize,
			page:make([]byte,0,pageSize),
			objects:make([]SlabObject,0,pageSize/currentSlabObjectSize),
		}
		for i:=0;i<len(s.objects);i++{
			so := &s.objects[i]
			so.mem = s.page[i*currentSlabObjectSize:(i+1)*currentSlabObjectSize]
			so.next = s.currentHead
			s.currentHead = unsafe.Pointer(so)
		}
		s.beginPtr = uintptr(unsafe.Pointer(s.objects[0].mem[0]))
		s.endPtr = uintptr(unsafe.Pointer(s.objects[len(s.objects)-1].mem[0]))
		pool.slabs = append(pool.slabs,s)

		// get next slab
		currentSlabObjectSize *= factor
		if currentSlabObjectSize > maxSize{
			break
		}
	}

	return pool
}

func (slab *Slab) Pop() []byte{
	var ptr unsafe.Pointer
	var so *SlabObject
	for {
		ptr = atomic.LoadPointer(&slab.currentHead)
		if ptr == nil{
			return nil
		}
		so = (*SlabObject)(ptr)
		if atomic.CompareAndSwapPointer(&slab.currentHead,so,so.next) {
			break
		}
	}
	so.next = nil
	return so.mem
}

func (slab *Slab) Push(mem []byte){
	ptr := (*reflect.SliceHeader)(unsafe.Pointer(&mem)).Data
	if slab.beginPtr <= ptr && slab.endPtr >= ptr{
		so := slab.objects[(ptr-slab.beginPtr)/uintptr(slab.eachObjectSize)]
		if so.next!=nil {
			panic("slab.Pool:Double free")
		}
		for {
			so.next = atomic.LoadPointer(&slab.currentHead)
			if atomic.CompareAndSwapPointer(&slab.currentHead,so.next,unsafe.Pointer(so)) {
				break
			}
		}
	}
}

func (pool *Pool) Alloc(size,capacity int)[]byte{
	if capacity<=pool.maxSize{
		if capacity < pool.minSize{
			capacity = pool.minSize
		}
		for i:=0;i<len(pool.slabs);i++{
			if pool.slabs[i].eachObjectSize >= capacity{
				mem := pool.slabs[i].Pop()
				if mem != nil{
					return mem[:size]
				}
			}
		}
	}
	return make([]byte,size,capacity)
}

func (pool *Pool) Free(mem []byte){
	capacity := cap(mem)
	for i:=0; i<len(pool.slabs);i++  {
		if pool.slabs[i].eachObjectSize >= capacity {
			pool.slabs[i].Push(mem)
			break
		}
	}
}