package Log
import (
	"time"
	"sync"
	"os"
	"log"
	"runtime"
	"strconv"
	"fmt"
)

const (
	Version string = "1.0.0"
)

type LEVEL int32
const DATEFORMAT = "2006-01-02"
const (
	_       = iota
	KB uint64 = 1 << (iota * 10)
	MB
	GB
	TB
)

const (
	ALL LEVEL = iota
	DEBUG
	INFO
	WARN
	ERROR
	FATAL
	OFF
)

type LogFile struct {
	dir string
	filename string
	_suffix int
	isCover bool
	_date *time.Time
	mu    *sync.RWMutex
	file  *os.File
	logger_real *log.Logger
}

func (f *LogFile) isMustRename() bool {
	if dailyRolling {
		t,_:=time.Parse(DATEFORMAT,time.Now().Format(DATEFORMAT))
		if t.After(*f._date) {
			return true
		}
	}else{
		if maxFileCount > 1{
			if fileSize(f.dir+"/"+f.filename) >= int64(maxFileSize) {
				return true
			}
		}
	}
	return false
}

func (f *LogFile)rename(){
	if dailyRolling{
		fn := f.dir+"/"+f.filename+"."+f._date.Format(DATEFORMAT)
		if !isExist(fn) && f.isMustRename(){
			if f.file != nil {
				f.file.Close()
			}
			err := os.Rename(f.dir+"/"+f.filename, fn)
			if err != nil {
				f.logger_real.Println("rename err", err.Error())
			}
			t,_ := time.Parse(DATEFORMAT,time.Now().Format(DATEFORMAT))
			f._date = &t
			f.file,_ = os.Create(f.dir+"/"+f.filename)
			f.logger_real = log.New(f.file,"\n",log.Ldate|log.Ltime|log.Lshortfile)
		}
	}else {
		f.coverNextOne()
	}
}

func (f *LogFile) nextSuffix() int {
	return int(f._suffix%int(maxFileCount) + 1)
}

func (f* LogFile)coverNextOne(){
	f._suffix = f.nextSuffix()
	if f.file != nil {
		f.file.Close()
	}
	if isExist(f.dir+"/"+f.filename+"."+strconv.Itoa(int(f._suffix))){
		os.Remove(f.dir + "/" + f.filename + "." + strconv.Itoa(int(f._suffix)))
	}
	os.Rename(f.dir+"/"+f.filename, f.dir+"/"+f.filename+"."+strconv.Itoa(int(f._suffix)))
	f.file ,_ = os.Create(f.dir+"/"+f.filename)
	f.logger_real = log.New(f.file,"\n",log.Ldate|log.Ltime|log.Lshortfile)
}

// 日志等级
var logLevel LEVEL = DEBUG
// 单个文件大小
var maxFileSize uint64
// 最大文件数
var maxFileCount int32
// 是否是按日期切换文件
var dailyRolling bool = true
// 是否也写到控制台
var consoleToo bool = true
// 是否按文件大小切换文件
var fileSizeRolling bool = false
// 日志行为者
var logger *LogFile
// 不同级别日志头
var LogMsgArray []string

func init(){
	LogMsgArray = []string{"all","debug","info","warn","error","fatal","off"}
}

func SetConsole(isConsole bool) {
	consoleToo = isConsole
}

func SetLevel(_level LEVEL) {
	logLevel = _level
}

// 按文件大小滚动文件
func SetRollingFile(fileDir, fileName string, maxNumber int32, maxSize uint64, _unit uint64) {
	maxFileCount = maxNumber
	maxFileSize = maxSize * _unit
	fileSizeRolling = true
	dailyRolling = false
	logger = &LogFile{dir:fileDir,filename:fileName,isCover:false,mu:new(sync.RWMutex)}
	logger.mu.Lock()
	for i:=1; i<int(maxFileCount);i++  {
		if isExist(logger.dir+"/"+logger.filename+strconv.Itoa(i)){
			logger._suffix = i
		}else {
			break
		}
	}
	if !logger.isMustRename(){
		logger.file,_ = os.OpenFile(logger.dir+"/"+logger.filename,os.O_RDWR|os.O_APPEND|os.O_CREATE, 0666)
		logger.logger_real = log.New(logger.file,"", log.Ldate|log.Ltime|log.Lshortfile)
	}else{
		logger.rename()
	}
	go fileMonitor()
	logger.mu.Unlock()
}

// 按日期滚动文件
func SetRollingDaily(fileDir, fileName string){
	dailyRolling = true
	fileSizeRolling = false
	t,_ := time.Parse(DATEFORMAT, time.Now().Format(DATEFORMAT))
	logger = &LogFile{dir:fileDir,filename:fileName,isCover:false,mu:new(sync.RWMutex),_date: &t}
	logger.mu.Lock()
	if !logger.isMustRename() {
		logger.file, _ = os.OpenFile(fileDir+"/"+fileName, os.O_RDWR|os.O_APPEND|os.O_CREATE, 0666)
		logger.logger_real = log.New(logger.file, "", log.Ldate|log.Ltime|log.Lshortfile)
	} else {
		logger.rename()
	}
	go fileMonitor()
	logger.mu.Unlock()
}

func console(s ...interface{}){
	if consoleToo{
		_,file,line,_ := runtime.Caller(2)
		short:=file
		for i:=len(file)-1;i>0;i--{
			if file[i] == '/' {
				short = file[i+1:]
				break
			}
		}
		file = short
		log.Println(file,strconv.Itoa(line),s)
	}
}

func catchError(){
	if err := recover();err != nil{
		log.Println("err",err)
	}
}

//
func fileCheck(){
	if logger != nil{
		if logger.isMustRename(){
			logger.mu.Lock()
			logger.rename()
			logger.mu.Unlock()
		}
	}
}

func logBehavior(level LEVEL,v ...interface{}){
	if logLevel <= level {
		if dailyRolling {
			fileCheck()
		}
		defer catchError()
		if logger != nil{
			logger.mu.RLock()
		}
		if logger != nil {
			logger.logger_real.Output(2,fmt.Sprintln(LogMsgArray[level],v))
		}
		if logger != nil{
			logger.mu.RUnlock()
		}
	}
}

func All(v ...interface{}){
	logBehavior(ALL,v)
}

func Debug(v ...interface{}){
	logBehavior(DEBUG,v)
}

func Info(v ...interface{}){
	logBehavior(INFO,v)
}

func Warn(v ...interface{}){
	logBehavior(WARN,v)
}

func Error(v ...interface{}){
	logBehavior(ERROR,v)
}

func Fatal(v ...interface{}){
	logBehavior(FATAL,v)
}

// CHECK FILE EXIST OR NOT
func isExist(path string) bool {
	_, err := os.Stat(path)
	return err == nil || os.IsExist(err)
}

// GET FILE SIZE
func fileSize(file string) int64 {
	f, e := os.Stat(file)
	if e != nil {
		fmt.Println(e.Error())
		return 0
	}
	return f.Size()
}

// check file every second
func fileMonitor(){
	timer := time.NewTicker(1*time.Second)
	for {
		select{
		case <-timer.C:
			fileCheck()
		}
	}
}
