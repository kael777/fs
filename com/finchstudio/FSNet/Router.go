package FSNet
import (
)

type handleLinkDisconnected func(*ClientConn)
type HandlerFunc func(*ClientConn, interface{}) (interface{},uint16)
type ServerCommon struct {
	M                  map[uint16]HandlerFunc   // msg code,and handler function
	DisconnectCallback handleLinkDisconnected
}

var MainRouter ServerCommon

func RegisterFunc(name uint16,handler HandlerFunc) {
	MainRouter.M[name] = handler
}

func init(){
	MainRouter.M = make(map[uint16]HandlerFunc)
}