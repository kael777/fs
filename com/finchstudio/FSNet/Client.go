package FSNet

import (
	"net"
	"time"
	"io"
	"com/finchstudio/FSUtils/binary"
)

type ClientConn struct {
	ID        		uint32 	     //连接ID
	Conn      		*net.TCPConn //连接
	Connected 		bool 		 //连接状态
	TimeoutCount 	int          //超时次数
	T            	*time.Ticker //超时检测定时器
	Key 			[]byte       //dh加密时使用的密钥
	Toc          	chan int	 //心跳包的chan
}

func NewClientConn(id uint32)*ClientConn{
	tcpConn := new(ClientConn)
	tcpConn.ID = id
	tcpConn.Connected = false
	tcpConn.TimeoutCount = 0
	tcpConn.T = time.NewTicker(15 * time.Second)
	tcpConn.Toc = make(chan int, 1)
	// if use dh,key changed every times,if use other,key is global
	tcpConn.Key = make([]byte,32)
	return tcpConn
}

//runHeartbeat 处理心跳函数
func (conn * ClientConn)heartBeatCheck(){
	for  {
		select {
		case state := <-conn.Toc:
			if state == 0XFFFF {
				return
			}
			conn.TimeoutCount = state
		case <-conn.T.C:
			if conn.TimeoutCount>3 {
				conn.CloseClient()
				return 
			}else if conn.TimeoutCount > 0{
				conn.TimeoutCount++
			}else{
				break
			}
		}
	}
}

func (conn *ClientConn)CloseClient(){
	if MainRouter.DisconnectCallback != nil{
		MainRouter.DisconnectCallback(conn)
	}
	conn.Toc<-0XFFFF
	conn.Conn.Close()
}

func (conn *ClientConn)Send(data interface{},cmd uint16) error{
	encoded, err := Sk.Codec.Encode(data,cmd,conn.Key)
	if err != nil {
		return err
	}else{
		dataLenByte := []byte{0,0,0,0}
		binary.PutUint32LE(dataLenByte,uint32(len(encoded)))
		encoded = append(dataLenByte[:], encoded...)
		conn.Conn.SetWriteDeadline(time.Now().Add(time.Second * 7))
		_, err := conn.Conn.Write(encoded)
		return err
	}
	return err
}

func (conn* ClientConn)Connect(host string) error{
	c, err := net.Dial("tcp", host)
	if err != nil {
		return err
	}
	conn.Conn = c.(*net.TCPConn)
	conn.Connected = true
	return nil
}

/**
从conn中读取一个int32值
param：conn 客户端连接socket对象
result：int 读取的int32值
		int 错误编号
**/
func (conn* ClientConn)ReadInt() (int, int) {
	data, errCode := conn.Recv(4)
	if errCode != -1 {
		return 0, errCode
	}
	return int(binary.GetUint32LE(data)), -1
}

func (conn* ClientConn)Recv(readLen int) ([]byte, int) {
	dataBuf := make([]byte,readLen)
	var dataLenTag int
	for {
		tmpNum, err := conn.Conn.Read(dataBuf[dataLenTag:readLen])
		if err != nil {
			if err == io.EOF {
				return dataBuf, 110
			}
			if err == io.ErrUnexpectedEOF {
				return dataBuf, 110
			}
			return dataBuf, 110
		}
		dataLenTag += tmpNum
		if dataLenTag >= readLen {
			break
		}
	}
	return dataBuf, -1
}

func HandleSocketClient(conn *net.TCPConn,idx int){
	fsConn := NewClientConn(uint32(idx))
	fsConn.Conn = conn
	fsConn.Connected = true

	go fsConn.heartBeatCheck()
	for {
		length, err := fsConn.ReadInt()
		if err != -1 {
			goto exit
		}
		if length > 100000 || length < 0{
			goto exit
		}

		msg,err := fsConn.Recv(length)
		if err != -1 {
			goto exit
		}
		data,cmd_id,err2:=Sk.Codec.Decode(msg,fsConn.Key)
		if data != nil && cmd_id > 0 && err2 == nil{
			if function, ok := MainRouter.M[cmd_id];!ok || function == nil{
				goto exit
			}else{
				response,cmdback := function(fsConn,data)
				if response != nil{
					fsConn.Send(response,cmdback)
				}else{
					goto exit
				}
			}
		}else{
			goto exit
		}
	}
	exit:{
		fsConn.CloseClient()
		return
	}
}
