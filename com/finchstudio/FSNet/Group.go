package FSNet
import "sync"

type KEY_TYPE uint32
type Group struct  {
	Clients map[KEY_TYPE]*ClientConn
	m sync.RWMutex

	State interface{}
}

func NewGroup() *Group{
	return &Group{
		Clients: make(map[KEY_TYPE]*ClientConn),
	}
}

func (g *Group) Len() int {
	g.m.RLock()
	l := len(g.Clients)
	g.m.RUnlock()
	return l
}

func (g *Group) Get(key KEY_TYPE) *ClientConn {
	g.m.RLock()
	session, _ := g.Clients[key]
	g.m.RUnlock()
	return session
}

func (g *Group) Put(key KEY_TYPE,c *ClientConn){
	g.m.Lock()
	if session, exists := g.Clients[key]; exists {
		g.remove(key, session)
	}
	g.Clients[key] = c
	g.m.Unlock()
}

func (g *Group) remove(key KEY_TYPE, session *ClientConn) {
	session.CloseClient()
	delete(g.Clients, key)
}

func (g *Group) Remove(key KEY_TYPE) bool {
	g.m.Lock()
	session, exists := g.Clients[key]
	if exists {
		g.remove(key, session)
	}
	g.m.Unlock()
	return exists
}

func (g *Group)Each(cb func(*ClientConn)){
	g.m.Lock()
	for _,v:=range g.Clients{
		cb(v)
	}
	g.m.Unlock()
}

func (g *Group) Close() {
	g.m.Lock()
	for key, session := range g.Clients {
		g.remove(key, session)
	}
	g.m.Unlock()
}