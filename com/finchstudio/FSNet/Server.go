package FSNet
import (
	"net"
	"time"
	"com/finchstudio/FSCodec"
)

type ServerKit struct {
	Codec FSCodec.Codec
	Crypto FSCodec.Crypto
}

var Sk ServerKit

func init()  {
	Sk = ServerKit{}
//	Sk.Codec = new(FSCodec.JsonStringCodec)
//	Sk.Codec = new(FSCodec.ProtobufCodec)
	Sk.Codec = new(FSCodec.MsgpackCodec)
	// init crypto
	FSCodec.GlobalEncryptKey = []byte(`heisenberg`)
	FSCodec.Cryp = new(FSCodec.DoNothingCrypto)
}
const HEARTBEAT_CMD uint16  = 1
//HeartbeatCallback 心跳回调
func HeartbeatCallback(conn *ClientConn, input interface{}) (interface{},uint16) {
	conn.Toc <- 0
	return input,HEARTBEAT_CMD
}

func (srv *ServerKit) ServerMainSock(ip string,cleanup func(),disconect func(*ClientConn)){
	MainRouter.DisconnectCallback=disconect
	// 初始化连接监听器
	tcpAddr, _ := net.ResolveTCPAddr("tcp4",ip)
	listener, _ := net.ListenTCP("tcp", tcpAddr)
	srv.DoRouter(HEARTBEAT_CMD,HeartbeatCallback)
	i := 0
	for {
		conn, err := listener.AcceptTCP()
		if err != nil {
			return
		}
		i++
		go HandleSocketClient(conn, i)
		time.Sleep(1e8)
	}

	if cleanup != nil{
		cleanup()
	}
}

func (srv *ServerKit) DoRouter(cmdid uint16,cb HandlerFunc){
	RegisterFunc(cmdid,cb)
}